## SCF(腾讯云函数) PHP CGI 代理函数

https://cloud.tencent.com/product/scf



### 项目源起：

移动互联网时代，对于小型PHP项目来说，如何扛住瞬时高并发是一个尴尬的问题！

小型PHP项目普遍存在以下三个情况：

> 1. “能力有限”——小团队小项目没有专业运维人员；
> 1. “预算不足”——客户的服务器预算可能就是一年几千；
> 1. “流量未知”——客户推广还是不推广，用户转发还是不转发，像薛定谔的猫一样；


在这三个条件的约束下，很少有方案可以同时满足“简单”、“廉价”、“按量付费”的要求。

我曾寄希望于新浪云的SAE，但是新浪云用“账户等级”设置门槛，SAE要实现扛瞬时高并发，至少得升级到年费1W的“经济型”帐号。

后来Serverless兴起，Serverless可以说特别符合要求。遗憾的是，Serverless中PHP是以常驻内存的CLI的模式执行，原本大量基于PHP CGI模式下的库、框架、WEB系统，都不能直接使用，必须进行改造适配。

另一方面，大部分PHP开发者习惯了无需管理内存的开发方式，在CLI模式下，PHP开发者需要控制好变量的使用及释放，一定程度上抬高了Serverless的使用门槛。

所幸，[Laravel Vapor 提供了一个很好的解决思路](https://learnku.com/articles/36046)，我们可以利用Serverless开启一个php-cgi服务，再将函数接收到的请求数据，转发给php-cgi去处理，拿到响应数据后再返回给浏览器。

 **最终结果就是，我们可以在Serverless继续以传统PHP WEB的方式来开发，无需做多余的适配。** 



### 使用说明：

将项目代码上传到腾讯云函数中，修改index.php文件中main_handler函数的两个变量值：`$doc_root` 和 `$script_name`，用以指定PHP代码所在目录及默认执行文件。具体如下:

```
    // PHP代码目录
    // 可以是自动读取云函数当前的目录
    // $doc_root = dirname(__FILE__);
    // 也可以手动自行设定CFS文件挂载目录
    $doc_root = '/mnt';

    // 根据请求地址判断php执行文件名称，默认为index.php
    $script_name = 'index.php';
```

当云函数收到请求后，会根据请求路径去，分析`$script_name`文件名称，然后读取执行`$doc_root`目录下的`$script_name`文件，与普通php执行环境一致。

另外，可通过项目下的php.ini文件，控制php cgi的环境参数。php.ini文件内默认开启了redis扩展，为了节省内存其他扩展不开启，可自行删除注释符号开启扩展。

 **注意：** 使用本项目前，请先了解云函数的概念及使用方式，具体可参考：

[云函数 SCF](https://cloud.tencent.com/product/scf) 

[使用控制台创建函数](https://cloud.tencent.com/document/product/583/37509)

[开发指南 > PHP](https://cloud.tencent.com/document/product/583/17531)

[挂载 CFS 文件系统](https://cloud.tencent.com/document/product/583/46199)


### 其他参考

[聊聊 Vapor](https://learnku.com/articles/36046)

[十分钟上线-函数计算玩转 WordPress](https://developer.aliyun.com/article/640912)